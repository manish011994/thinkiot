import React from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SignUp} from '../SignUp/SignUp';
import {SignUpOTP} from '../SignUpOTP/SignUpOTP';
import { SignInScreen } from '../SignIn/SignIn';
import { ForgotPasswordScreen } from '../ForgotPassword/ForgotPassword';
import Language from '../Language/Language'
const { Navigator, Screen}  = createStackNavigator();

const MainNavigator = () => {
    return (
        <Navigator  headerMode="none"  initialRouteName="SignIn">
            <Screen name='SignUp' component={SignUp} ></Screen>
            <Screen name='SignUpOTP' component={SignUpOTP} ></Screen>
            <Screen name='SignIn' component={SignInScreen}/>
            <Screen name='ForgotPassword' component={ForgotPasswordScreen}/>
        </Navigator>
    )
}

export const  AppNavigator = () =>{
    return (
        <NavigationContainer>
            <MainNavigator />
        </NavigationContainer>
    )
}