import React from 'react'
import { SafeAreaView, View, StyleSheet, TouchableWithoutFeedback } from 'react-native'
import { TopNavigation, Layout, Text, Input, Button,useTheme } from '@ui-kitten/components'

export const SignUpOTP = ({ navigation }) => {

    const theme = useTheme();

    const navigateDetails = () => {
        navigation.navigate("SignUp")
    }
    const resendOtpAction = () => {
        alert('This is working')
    }


    return (
        <SafeAreaView style={{ flex: 1, }}>
            {/* <TopNavigation title='MyApp' alignment='center'/> */}
            <Layout style={{ flex: 1, paddingHorizontal: 15 }}>
                <View style={styles.header} >
                    <Text category='h1'>Verify Mobile No</Text>
                    <Text category='h5'>please enter OTP sent to 959466544</Text>
                </View>



                <View style={styles.inputField}>
                    <Input
                        placeholder='OTP'

                        caption=""
                    />
                </View>




                <Button style={styles.SignUpButton} onPress={navigateDetails}>Verify</Button>

                <View>
                    <Text> if you dont receive OTP, 
                    <Text onPress={resendOtpAction} style={{color:theme['color-primary-default']}}> Resend</Text>
                    </Text>
                </View>


            </Layout>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    header: {
        minHeight: 450,
        justifyContent: 'center',
        alignItems: 'center',

    },
    inputField: {
        // marginHorizontal: 16,
        paddingVertical: 5
    },
    SignUpButton: {
        marginVertical: 20,
        //   marginHorizontal: 16,
    }
})

