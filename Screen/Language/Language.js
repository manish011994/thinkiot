import React from 'react'
import { StyleSheet, SafeAreaView } from 'react-native';
import { TopNavigation, Divider, List, ListItem, Layout } from '@ui-kitten/components';
import i18n from 'i18n-js'
import memoize from 'lodash.memoize'
const data = new Array(
    {
        title: 'English',
        description: 'English Language US',
        code:'en-US'
    },
    {
        title: 'Hindi',
        description: 'Hindi from India',
        code:'hi-IN'
    })

const translate = memoize(
    (key, config) => i18n.t(key, config),
    (key, config) => (config ? key + JSON.stringify(config) : key)
)
export default Language = (props) => {



    const renderItem = ({ item, index }) => (
        <ListItem
            onPress={()=> {props.navigation.navigate('SignIn')}}
            title={`${item.title} `}
            description={`${translate('hello')}`}
        />
    );

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <TopNavigation title='Choose Language' alignment='center' />
            <Layout style={{ flex: 1, }}>
                <List
                    style={styles.container}
                    data={data}
                    ItemSeparatorComponent={Divider}
                    renderItem={renderItem}
                />
            </Layout>
        </SafeAreaView>
    );
};


const styles = StyleSheet.create({
    container: {
        // maxHeight: 200,
    },
});