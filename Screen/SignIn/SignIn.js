import React from 'react';
import { StyleSheet, SafeAreaView, View, TouchableWithoutFeedback } from 'react-native';
import { Button, Icon, Divider, Layout, Text, Input } from '@ui-kitten/components';
import ValidationComponent from 'react-native-form-validator';
import Toast from 'react-native-simple-toast';
import Database from '../../Database/Database';

const db = new Database();
const EmailIcon = (props) => (
    <Icon {...props} name='email'/>
  );
const GoogleIcon = (props) => (
    <Icon {...props} name='google'/>
);
const FbIcon = (props) => (
    <Icon {...props} name='facebook'/>
);
const TwitterIcon = (props) => (
    <Icon {...props} name='twitter'/>
);

export class SignInScreen extends ValidationComponent {
    constructor(props) {
        super(props)
        this.state = { secureTextEntry: true, email: "", password: "", userDetails: {} }
    }

async loginUser() {
    const { email, password } = this.state;
    try {
      let response = await fetch('http://192.168.0.207:3000/login', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email: email, password: password }),
      });
      let statusCode = response.status;
      let result = await response.json();
      console.log(result);
      if(statusCode == 200) {
        global.API_KEY = result.data.token;
        var userDetails = result.data.userDetails;
        userDetails.token = global.API_KEY;
        db.addUser(userDetails);
        Toast.show('User Logged In. Token - '+ global.API_KEY, Toast.LONG);
      } else if(statusCode == 404) {
        Toast.show(result.error, Toast.LONG);
      }
    }
    catch (error) {
      console.error(error)
    }
  }

    toggleSecureEntry = () => {
       const { secureTextEntry } = this.state;
       this.setState({ secureTextEntry: !secureTextEntry });
    };

    navigateForgotPassword = () => {
      this.props.navigation.navigate('ForgotPassword');
    };

    navigateSignUp = () => {
      this.props.navigation.navigate('SignUp');
    };

    renderIcon = (props) => (
        <TouchableWithoutFeedback onPress={this.toggleSecureEntry}>
            <Icon {...props} name={this.state.secureTextEntry ? 'eye-off' : 'eye'}/>
        </TouchableWithoutFeedback>
        );

    loginUserAction = () => {
        let validForm = this.validate({
          password: { required: true },
          email: { email: true, required: true },
        });

        if (validForm) {
          this.loginUser()
        } else {
          //alert("form not valid")
        }
    }
    componentDidMount() {
        db.listUser();
    }

    render() {
    const { userDetails } = this.state;
      return (
        <SafeAreaView style={{ flex: 1 }}>
          <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 15 }}>
              <React.Fragment>
                  <View style={styles.row}>
                    <Text style={styles.text} category='h1'>Think IOT</Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={styles.text} category='p2'>Sign in to your account</Text>
                  </View>
                  <View style={styles.row}>
                    <Input
                        ref="email"
                        style={[styles.input, styles.marginTop40]}
                        status={this.isFieldInError("email") ? "danger" : ""}
                        placeholder='Email'
                        value={userDetails.email}
                        accessoryRight={EmailIcon}
                        onChangeText={(email) => this.setState({ email })}
                        />
                  </View>
                  <View style={styles.row}>
                  <Input
                    ref="password"
                    style={[styles.input, styles.marginTop10]}
                    status={this.isFieldInError("password") ? "danger" : ""}
                    placeholder='Password'
                    value={userDetails.password}
                    accessoryRight={this.renderIcon}
                    secureTextEntry={this.state.secureTextEntry}
                    onChangeText={password => this.setState({ password })}
                    />
                  </View>
                  <View style={styles.row}>
                    <View style={styles.row2}>
                        <View style={styles.flex1}>
                            <Text style={[styles.text, styles.textRight]} onPress={this.navigateForgotPassword} category='s2'>Forgot your password?</Text>
                        </View>
                    </View>
                  </View>
                  <View style={[styles.row, styles.marginTop60]}>
                    <View style={styles.flex1}>
                        <Button style={styles.stretch} onPress={this.loginUserAction} size='large'>
                            SIGN IN
                        </Button>
                    </View>
                  </View>
                  <View style={[styles.row, styles.marginTop40]}>
                    <Text style={styles.text} category='p2'>Or Sign In using Social Media</Text>
                  </View>
                  <View style={[styles.row, styles.marginTop40]}>
                    <View style={[styles.container, styles.stretch]}>
                    <Button style={styles.button} appearance='ghost' status='danger' accessoryLeft={GoogleIcon}/>
                    <Button style={styles.button} appearance='ghost' status='danger' accessoryLeft={FbIcon}/>
                    <Button style={styles.button} appearance='ghost' status='danger' accessoryLeft={TwitterIcon}/>
                    </View>
                  </View>
                  <View style={[styles.row, styles.marginTop20]}>
                    <Text style={styles.text} category='s2' onPress={this.navigateSignUp}>Don't have an account? Sign Up</Text>
                  </View>
              </React.Fragment>
          </Layout>
        </SafeAreaView>
      );
  }
};

const styles = StyleSheet.create({
    row: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    row2: {
      flexDirection: 'row',
      flex: 1,
    },
    flex1: {
      flex: 1,
    },
    text: {
      margin: 2,
    },
    textRight: {
        textAlign: 'right',
        alignSelf: 'stretch',
    },
    input: {
      flex: 1,
      margin: 2,
    },
    marginTop40: {
        marginTop: 40,
    },
    marginTop60: {
        marginTop: 60,
    },
    marginTop10: {
        marginTop: 10,
    },
    marginTop20: {
        marginTop: 20,
    },
    stretch : {
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
  });