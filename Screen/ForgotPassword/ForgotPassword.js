import React from 'react';
import { StyleSheet, SafeAreaView, View, TouchableWithoutFeedback } from 'react-native';
import { Button, Icon, Divider, Layout, Text, Input } from '@ui-kitten/components';
import ValidationComponent from 'react-native-form-validator';
import Toast from 'react-native-simple-toast';

const EmailIcon = (props) => (
    <Icon {...props} name='email'/>
  );

export class ForgotPasswordScreen extends ValidationComponent {
    constructor(props) {
        super(props)
        this.state = { email: "", userDetails: {} }
    }

async forgotPassword() {
    const { email } = this.state;
    try {
      let response = await fetch('http://192.168.0.207:3000/forgotPassword', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email: email }),
      });
      let json = await response.json();
        console.log(json);
      if (json.result == "success") {
        Toast.show('Forgot Password Link sent to - '+ email, Toast.LONG);
      } else {
        alert(JSON.stringify(json.msg));
      }
    }
    catch (error) {
      console.error(error)
    }
  }

forgotPasswordAction = () => {
        let validForm = this.validate({
          email: { email: true, required: true },
        });

        if (validForm) {
          this.forgotPassword()
        } else {
          //alert("form not valid")
        }
    }

render() {
  const { userDetails } = this.state;
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 15 }}>
          <React.Fragment>
              <View style={styles.row}>
                <Text style={styles.text} category='h1'>Forgot Password</Text>
              </View>
              <View style={styles.row}>
                <Text style={[styles.text, styles.marginTop60]} category='p2'>Please enter your email address</Text>
              </View>
              <View style={styles.row}>
                <Input
                    style={[styles.input, styles.marginTop40]}
                    placeholder='Email'
                    status={this.isFieldInError("email") ? "danger" : ""}
                    onChangeText={(email) => this.setState({ email })}
                    accessoryRight={this.EmailIcon}
                    />
              </View>
              <View style={[styles.row, styles.marginTop60]}>
                <View style={styles.flex1}>
                    <Button style={styles.stretch} size='large' onPress={this.forgotPasswordAction}>
                        RESET PASSWORD
                    </Button>
                </View>
              </View>
          </React.Fragment>
      </Layout>
    </SafeAreaView>
  );
  }
};

const styles = StyleSheet.create({
    row: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    row2: {
      flexDirection: 'row',
      flex: 1,
    },
    flex1: {
      flex: 1,
    },
    text: {
      margin: 2,
    },
    textRight: {
        textAlign: 'right',
        alignSelf: 'stretch',
    },
    input: {
      flex: 1,
      margin: 2,
    },
    marginTop40: {
        marginTop: 40,
    },
    marginTop60: {
        marginTop: 60,
    },
    marginTop10: {
        marginTop: 10,
    },
    marginTop20: {
        marginTop: 20,
    },
    stretch : {
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
  });