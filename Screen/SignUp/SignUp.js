import React, { Component } from 'react'
import { SafeAreaView, StyleSheet, View, TouchableWithoutFeedback } from 'react-native'

import { TopNavigation, Layout, Text, Button, Input, Divider, Icon } from '@ui-kitten/components'
import ValidationComponent from 'react-native-form-validator';

const AlertIcon = (props) => (
  <Icon {...props} name='alert-circle-outline' />
);

export class SignUp extends ValidationComponent {
  constructor(props) {
    super(props)
    this.state = { secureTextEntry: true, email: "", mobile: "", password: "", userDetails: {} }
  }

  async registerUser() {
    const { email, mobile, password } = this.state
    try {
      let response = await fetch('http://192.168.0.207:3000/signup', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email: email, password: password, mobile: mobile }),
      });
      let json = await response.json();
        console.log(json);
      if (json.status == "success") {
        this.props.navigation.navigate("SignUpOTP")
      } else {
        alert(JSON.stringify(json.msg));
      }
    }
    catch (error) {
      console.error(error)
    }
  }

  navigateDetails = () => {
    //const {email,password,mobile} = this.state
    let validForm = this.validate({
      password: { minlength: 8, maxlength: 20, required: true },
      email: { email: true, required: true },
      mobile: { numbers: true, required: true },

    });
    if (validForm) {
      this.registerUser()
    } else {
      //alert("form not valid")
    }
  }
  renderIcon = (props) => (
    <TouchableWithoutFeedback onPress={this.toggleSecureEntry}>
      <Icon {...props} name={this.state.secureTextEntry ? 'eye-off' : 'eye'} />
    </TouchableWithoutFeedback>
  );

  toggleSecureEntry = () => {
    const { secureTextEntry } = this.state;
    this.setState({ secureTextEntry: !secureTextEntry });
  };

  render() {
    const { userDetails } = this.state
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Layout style={{ flex: 1, paddingHorizontal: 15 }}>
          <View style={styles.header} >
            <Text category='h1'>Think IOT</Text>
            <Text category='h5'>Create Account</Text>
          </View>

          <View style={styles.inputField}>
            <Input ref="email"
              status={this.isFieldInError("email") ? "danger" : ""}
              placeholder='Email'
              onChangeText={(email) => this.setState({ email })}
            />
          </View>
          <View style={styles.inputField}>
            <Input ref="mobile"
              status={this.isFieldInError("mobile") ? "danger" : ""}
              placeholder='Mobile No'
              caption="Mobile verification will be done in next step"
              onChangeText={(mobile) => this.setState({ mobile })}
            />
          </View>
          <View style={styles.inputField}>
            <Input ref="password"
              value={userDetails.password}
              status={this.isFieldInError("password") ? "danger" : ""}
              placeholder='Password'
              caption='Should contain at least 8 symbols'
              accessoryRight={this.renderIcon}
              captionIcon={AlertIcon}
              secureTextEntry={this.state.secureTextEntry}
              onChangeText={password => this.setState({ password })}
            />
          </View>
          <Button style={styles.SignUpButton} onPress={this.navigateDetails}>SignUp</Button>
        </Layout>

      </SafeAreaView>
    )
  }


}


const styles = StyleSheet.create({
  header: {
    minHeight: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputField: {
    paddingVertical: 10
  },
  SignUpButton: {
    marginVertical: 20,
  }
})
