import SQLite from "react-native-sqlite-storage";
SQLite.DEBUG(true);
SQLite.enablePromise(true);

const database_name = "thinkiot.db";
const database_version = "1.0";
const database_displayname = "Thinkinnotech IOT";
const database_size = 200000;

export default class Database {
initDB() {
  let db;
  return new Promise((resolve) => {
    console.log("Plugin integrity check ...");
    SQLite.echoTest()
      .then(() => {
        console.log("Integrity check passed ...");
        console.log("Opening database ...");
        SQLite.openDatabase(
          database_name,
          database_version,
          database_displayname,
          database_size
        )
          .then(DB => {
            db = DB;
            console.log("Database OPEN");
            db.executeSql('SELECT 1 FROM user LIMIT 1').then(() => {
                console.log("Database is ready ... executing query ...");
            }).catch((error) =>{
                console.log("Received error: ", error);
                console.log("Database not yet ready ... populating data");
                db.transaction((tx) => {
                    tx.executeSql('CREATE TABLE IF NOT EXISTS user (userId, userFirstName, userLastName, loggedInStatus, token, userMPIN)');
                }).then(() => {
                    console.log("Table created successfully");
                }).catch(error => {
                    console.log(error);
                });
            });
            resolve(db);
          })
          .catch(error => {
            console.log(error);
          });
      })
      .catch(error => {
        console.log("echoTest failed - plugin not functional");
      });
    });
};

closeDatabase(db) {
  if (db) {
    console.log("Closing DB");
    db.close()
      .then(status => {
        console.log("Database CLOSED");
      })
      .catch(error => {
        this.errorCB(error);
      });
  } else {
    console.log("Database was not OPENED");
  }
};

listUser() {
  return new Promise((resolve) => {
    const user = [];
    this.initDB().then((db) => {
      db.transaction((tx) => {
        tx.executeSql('SELECT * FROM user u', []).then(([tx,results]) => {
          console.log("Query completed");
          var len = results.rows.length;
          for (let i = 0; i < len; i++) {
            let row = results.rows.item(i);
            console.log(`User ID: ${row.user_id}, User Name: ${row.first_name}`)
            const { user_id, first_name, last_name, status } = row;
            user.push({
              user_id,
              first_name,
              last_name,
              status
            });
          }
          console.log(user);
          resolve(user);
        });
      }).then((result) => {
        this.closeDatabase(db);
      }).catch((err) => {
        console.log(err);
      });
    }).catch((err) => {
      console.log(err);
    });
  });
}

addUser(user) {
  return new Promise((resolve) => {
    this.initDB().then((db) => {
      db.transaction((tx) => {
        tx.executeSql('INSERT INTO User (userId, userFirstName, userLastName, loggedInStatus, token, userMPIN) VALUES (?, ?, ?, ?, ?, ?)',
         [user.user_id, user.first_name, user.last_name, user.status, user.token, '0']).then(([tx, results]) => {
          resolve(results);
          console.log("User Added");
        });
      }).then((result) => {
        this.closeDatabase(db);
      }).catch((err) => {
        console.log(err);
      });
    }).catch((err) => {
      console.log(err);
    });
  });
}
deleteUser(id) {
  return new Promise((resolve) => {
    this.initDB().then((db) => {
      db.transaction((tx) => {
        tx.executeSql('DELETE FROM user WHERE userId = ?', [id]).then(([tx, results]) => {
          console.log(results);
          resolve(results);
        });
      }).then((result) => {
        this.closeDatabase(db);
      }).catch((err) => {
        console.log(err);
      });
    }).catch((err) => {
      console.log(err);
    });
  });
}


}